﻿using Asteroids.Core;

namespace Asteroids.Core
{
    public interface IView<T> where T : Actor
    {
        void SetActor(T actor);
    }

    public class ActorView<T> : PoolableObject, IView<T> where T : Actor
    {
        protected T actor;

        public void SetActor(T actor)
        {
            this.actor = actor;

            transform.position = actor.Position;
            transform.rotation = actor.Rotation;
            transform.localScale = actor.Scale;

            actor.Disposed += OnActorDisposed;
        }

        private void OnActorDisposed()
        {
            Release();
        }
    }
}

