﻿using UnityEngine;

namespace Asteroids.Core
{
    public class Transformable
    {
        public Vector3 Position { get; set; } = Vector3.zero;
        public Quaternion Rotation { get; set; } = Quaternion.identity;
        public Vector3 Scale { get; set; } = Vector3.one;

        public Vector3 Up()
        {
            Matrix4x4 rotationMatrix = Matrix4x4.Rotate(Rotation);
            Vector3 upVector = rotationMatrix.MultiplyVector(Vector3.up);

            return upVector;
        }
    }
}