﻿using Asteroids.Core.Physics;
using System.Collections.Generic;

namespace Asteroids.Core
{
    public class World
    {
        public PhysicsEngine Physics { get; private set; }
        public MessageBroker MessageBroker { get; private set; }
        
        private HashSet<Actor> actors;
        private HashSet<Actor> actorsToDestroy;

        public World(PhysicsEngine physics, MessageBroker messageBroker)
        {
            this.Physics = physics;
            this.MessageBroker = messageBroker;
            this.actors = new HashSet<Actor>();
            this.actorsToDestroy = new HashSet<Actor>();
        }

        public T AddActor<T>(T actor) where T : Actor
        {
            actors.Add(actor);
            return actor;
        }

        public void DestroyActor(Actor actor) 
        {
            actorsToDestroy.Add(actor);
        }
        private void DestroyActorInternal(Actor actor)
        {
            actors.Remove(actor);
            actor.Dispose();
            actor = null;
        }

        public void Simulate(float deltaTime)
        {
            Physics.Simulate(deltaTime);

            foreach (var actor in actors)
            {
                actor.OnUpdate(deltaTime);
                actor.UpdateComponents(deltaTime);
            }

            foreach(var actor in actorsToDestroy)
            {
                DestroyActorInternal(actor);
            }
            actorsToDestroy.Clear();
        }
    }
}