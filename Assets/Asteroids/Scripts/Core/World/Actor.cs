﻿using Asteroids.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Asteroids.Core
{

    public class Actor : Transformable, IDisposable
    {
        protected World world;

        private List<ActorComponent> components = new List<ActorComponent>();

        public Action Disposed;

        public Actor(World world)
        {
            this.world = world;

            world.AddActor(this);
        }

        public T AddComponent<T>() where T : ActorComponent, new()
        {
            T component = new T();
            component.Owner = this;
            component.World = world;
            components.Add(component);

            component.OnAdd();

            return component;
        }

        public T AddComponent<T>(T component, World world) where T : ActorComponent
        {
            component.Owner = this;
            component.World = world;

            components.Add(component);

            component.OnAdd();

            return component;
        }

        public T GetComponent<T>() where T : ActorComponent
        {
            var comp = components.FirstOrDefault(x => x is T);

            return (T)comp ?? null;
        }

        public void UpdateComponents(float deltaTime)
        {
            foreach (ActorComponent component in components)
            {
                component.OnUpdate(deltaTime);
            }
        }

        public virtual void Dispose()
        {
            foreach (ActorComponent component in components)
            {
                component.OnDestroy();
            }

            Disposed?.Invoke();
        }

        public virtual void OnCollisionEnter(Actor other) { }

        public virtual void OnUpdate(float deltaTime) { }
    }
}