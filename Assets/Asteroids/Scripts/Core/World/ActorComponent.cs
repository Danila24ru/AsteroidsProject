﻿using Asteroids.Scripts;

namespace Asteroids.Core
{
    public class ActorComponent
    {
        public World World { get; set; }
        public Actor Owner { get; set; }

        public ActorComponent() { }

        public virtual void OnAdd() { }
        public virtual void OnUpdate(float deltaTime) { }
        public virtual void OnDestroy() { }
    }
}