﻿
using Asteroids.Gameplay;
using Asteroids.Gameplay.Enemies;
using Asteroids.Gameplay.Weapons;
using Asteroids.UI;
using System;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;

namespace Asteroids.Core.ContentProvider
{

    public class ViewFactory
    {
        private readonly PrefabsProvider prefabsProvider;

        private PooledObject<EnemyView> brickPool;
        private PooledObject<EnemyView> brickPartPool;
        private PooledObject<EnemyView> saucerPool;
        private PooledObject<ProjectileView> projectilePool;
        private PooledObject<LaserView> laserPool;

        public ViewFactory(PrefabsProvider prefabsProvider)
        {
            this.prefabsProvider = prefabsProvider;

            brickPool = new PooledObject<EnemyView>(prefabsProvider.GetBrickPrefab());
            brickPartPool = new PooledObject<EnemyView>(prefabsProvider.GetBrickPartPrefab());
            saucerPool = new PooledObject<EnemyView>(prefabsProvider.GetSaucerPrefab());
            projectilePool = new PooledObject<ProjectileView>(prefabsProvider.GetProjectilePrefab());
            laserPool = new PooledObject<LaserView>(prefabsProvider.GetLaserView());
        }

        public PlayerView SpawnPlayerView(Player player)
        {
            var playerView = UnityEngine.Object.Instantiate(prefabsProvider.GetPlayerPrefab());
            playerView.SetActor(player);
            return playerView;
        }

        public EnemyView SpawnRandomEnemyView(Vector3 position, Quaternion rotation)
        {
            var enemy = UnityEngine.Random.Range(1, 3) % 2 != 0 ? brickPool.Get() : saucerPool.Get();
            enemy.transform.SetPositionAndRotation(position, rotation);
            return enemy;
        }

        public EnemyView SpawnAsteroidView(Enemy enemy)
        {
            var view = brickPool.Get();
            view.SetActor(enemy);
            return view;
        }

        public EnemyView SpawnSaucerView(ChaserEnemy enemy)
        {
            var view = saucerPool.Get();
            view.SetActor(enemy);
            return view;
        }

        public EnemyView SpawnPartView(Enemy enemy)
        {
            var view = brickPartPool.Get();
            view.SetActor(enemy);
            return view;
        }

        public ProjectileView SpawnProjectileView(Projectile projectile)
        {
            var view = projectilePool.Get();
            view.SetActor(projectile);
            return view;
        }

        public LaserView SpawnLaserView(Vector3 position, Quaternion rotation)
        {
            var laser = laserPool.Get();
            var line = laser.GetComponent<LineRenderer>();
            line.SetPositions(new[] { position, (rotation * Vector3.up) * laser.laserLength });
            return laser;
        }

        public GameUI SpawnGameUI() => UnityEngine.Object.Instantiate(prefabsProvider.GetGameUI());
        public LaserWeaponUI SpawnLaserWeaponUI(LaserWeapon laserWeapon)
        {
            var ui = UnityEngine.Object.Instantiate(prefabsProvider.GetLaserWeaponUI());
            ui.Init(laserWeapon);
            return ui;
        }
    }
}