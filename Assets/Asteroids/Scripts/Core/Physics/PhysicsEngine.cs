using System;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroids.Core.Physics
{
    public struct LinecastResult
    {
        public PhysicsCollider collider;
        public Vector2 hitPoint;
    }

    public class PhysicsCollision
    {
        public PhysicsCollider a;
        public PhysicsCollider b;
    }

    public class PhysicsEngine
    {
        private List<PhysicsCollider> colliders;
        private HashSet<PhysicsBody> bodies;
        private List<PhysicsCollision> collisions;

        public PhysicsEngine()
        {
            colliders = new List<PhysicsCollider>();
            bodies = new HashSet<PhysicsBody>();
            collisions = new List<PhysicsCollision>();
        }

        public void Simulate(float deltaTime)
        {
            UpdateBodies(deltaTime);
            CheckCollisions();
            InvokeCollisions();
        }

        public void AddCollider(PhysicsCollider collider) => colliders.Add(collider);
        public void RemoveCollider(PhysicsCollider collider) => colliders.Remove(collider);

        public void AddBody(PhysicsBody body) => bodies.Add(body);
        public void RemoveBody(PhysicsBody body) => bodies.Remove(body);

        private void UpdateBodies(float deltaTime)
        {
            foreach(var body in bodies)
            {
                body.Owner.Position += body.Velocity * deltaTime;
            }
        }

        private void CheckCollisions()
        {
            for (int i = 0; i < colliders.Count; i++)
            {
                for(int j = i + 1; j < colliders.Count; j++)
                {
                    if (colliders[i].Layer == colliders[j].Layer)
                        continue;

                    switch (colliders[i].Shape, colliders[j].Shape)
                    {
                        case (ColliderShape.Circle, ColliderShape.Circle):
                            CheckCircleToCircleCollision(colliders[i], colliders[j]);
                            break;
                    }
                }
            }
        }

        private void InvokeCollisions()
        {
            if(collisions.Count == 0)
            {
                return;
            }

            for (int i = 0; i < collisions.Count; i++)
            {
                collisions[i].a.Owner.OnCollisionEnter(collisions[i].b.Owner);
                collisions[i].b.Owner.OnCollisionEnter(collisions[i].a.Owner);
            }
            collisions.Clear();
        }

        private void CheckCircleToCircleCollision(PhysicsCollider colliderA, PhysicsCollider colliderB)
        {
            if ((colliderA.Owner.Position - colliderB.Owner.Position).sqrMagnitude
                < colliderA.Radius * colliderA.Radius + colliderB.Radius * colliderB.Radius)
            {
                collisions.Add(new PhysicsCollision() { a = colliderA, b = colliderB });
            }
        }

        public bool CastLine(Vector2 start, Vector2 end, ref LinecastResult[] linecastResult, out int totalHits)
        {
            var maxResults = linecastResult.Length;
            totalHits = 0;

            if (maxResults == 0)
                return false;

            for (int i = 0; i < colliders.Count; i++)
            {
                var (isHit, points) = PhysicsMath.CheckLineToCircleCollision(start, end, colliders[i]);

                if (isHit)
                {
                    linecastResult[totalHits].collider = colliders[i];
                    linecastResult[totalHits].hitPoint = points[0];
                    totalHits++;

                    if (totalHits >= maxResults)
                        return true;
                }
            }

            return totalHits > 0;
        }
    }
}
