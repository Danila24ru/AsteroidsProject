using UnityEngine;

namespace Asteroids.Core.Physics
{
    public static class PhysicsMath
    {
        private const float EPS = 0.000001f;

        public static (bool, Vector2[]) CheckLineToCircleCollision(Vector2 start, Vector2 end, PhysicsCollider circleCollider)
        {
            var collisionPoints = GetLineCircleIntersections(start, end, circleCollider);

            if (collisionPoints == null)
                return (false, null);

            var p1 = collisionPoints[0];
            var includeP1 = IsPointInsideRectangle(p1, start, end);

            if (collisionPoints.Length == 1)
            {
                if (includeP1) return (true, new[] { p1 });
                return (false, null);
            }

            var p2 = collisionPoints[1];
            var includeP2 = IsPointInsideRectangle(p2, start, end);

            if (includeP1 && includeP2)
                return (true, new[] { p1, p2 });
            if (includeP1)
                return (true, new[] { p1 });
            if (includeP2)
                return (true, new[] { p2 });

            return (false, null);
        }

        private static Vector2[] GetLineCircleIntersections(Vector2 start, Vector2 end, PhysicsCollider circle)
        {
            var a = start.y - end.y;
            var b = end.x - start.x;
            var c = end.x * start.y - start.x * end.y;

            var x = circle.Owner.Position.x;
            var y = circle.Owner.Position.y;
            var r = circle.Radius;

            //NormalizeLine(ref a, ref b, ref c);

            var A = a * a + b * b;
            var B = 2 * a * b * y - 2 * a * c - 2 * b * b * x;
            var C = b * b * x * x + b * b * y * y - 2 * b * c * y + c * c - b * b * r * r;

            var D = B * B - 4 * A * C;
            float x1, y1, x2, y2;

            if (Mathf.Abs(b) < EPS)
            {
                x1 = c / a;

                if (Mathf.Abs(x - x1) > r) return null;

                if (Mathf.Abs((x1 - r) - x) < EPS || Mathf.Abs((x1 + r) - x) < EPS)
                    return new[] { new Vector2(x1, y) };

                var dx = Mathf.Abs(x1 - x);
                var dy = Mathf.Sqrt(r * r - dx * dx);

                return new[] { new Vector2(x1, y + dy), new Vector2(x1, y - dy) };
            }
            else if (Mathf.Abs(D) < EPS)
            {
                x1 = -B / (2 * A);
                y1 = (c - a * x1) / b;

                return new[] { new Vector2(x1, y1) };
            }
            else if (D < 0)
            {
                return null;
            }
            else
            {
                D = Mathf.Sqrt(D);

                x1 = (-B + D) / (2 * A);
                y1 = (c - a * x1) / b;

                x2 = (-B - D) / (2 * A);
                y2 = (c - a * x2) / b;

                return new[] { new Vector2(x1, y1), new Vector2(x2, y2) };
            }
        }

        private static void NormalizeLine(ref float a, ref float b, ref float c)
        {
            if (Mathf.Abs(b) < EPS)
            {
                c /= a;
                a = 1;
                b = 0;
            }
            else
            {
                a = (Mathf.Abs(a) < EPS) ? 0 : a / b;
                c /= b;
                b = 1;
            }
        }

        private static bool IsPointInsideRectangle(Vector2 point, Vector2 start, Vector2 end)
        {
            var x = Mathf.Min(start.x, end.x);
            var X = Mathf.Max(start.x, end.x);
            var y = Mathf.Min(start.y, end.y);
            var Y = Mathf.Max(start.y, end.y);
            return x - EPS <= point.x && point.x <= X + EPS &&
                   y - EPS <= point.y && point.y <= Y + EPS;
        }
    }
}


