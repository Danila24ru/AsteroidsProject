﻿using System;
using UnityEngine;

namespace Asteroids.Core.Physics
{
    public class PhysicsBody : ActorComponent
    {
        public Vector3 Velocity { get; set; }

        public PhysicsBody()
        {
            Velocity = Vector3.zero;
        }

        public PhysicsBody(Vector3 initialVelocity)
        {
            Velocity = initialVelocity;
        }

        public override void OnAdd()
        {
            World.Physics.AddBody(this);
        }

        public override void OnDestroy()
        {
            World.Physics.RemoveBody(this);
        }
    }
}