using System;
using UnityEngine;

namespace Asteroids.Core.Physics
{
    public enum ColliderShape
    {
        Circle,
    }

    public class PhysicsCollider : ActorComponent
    {
        public int Layer { get; set; }
        public ColliderShape Shape { get; set; }
        public float Radius { get; set; } = 1.0f;

        public override void OnAdd()
        {
            World.Physics.AddCollider(this);
        }

        public override void OnDestroy()
        {
            World.Physics.RemoveCollider(this);
        }
    }
}