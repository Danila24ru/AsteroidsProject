﻿
using Asteroids.Gameplay;
using Asteroids.Gameplay.Enemies;
using Asteroids.Gameplay.Inputs;
using Asteroids.Gameplay.Weapons;
using UnityEngine;

namespace Asteroids.Core.ContentProvider
{
    public class ActorsFactory
    {
        private readonly ViewFactory viewFactory;
        private readonly World world;
        private readonly MessageBroker messageBroker;

        public ActorsFactory(ViewFactory viewFactory, World world, MessageBroker messageBroker) 
        {
            this.viewFactory = viewFactory;
            this.world = world;
            this.messageBroker = messageBroker;
        }

        public Player CreateDefaultPlayer(PlayerController playerController)
        {
            var laserWeapon = new LaserWeapon(viewFactory, messageBroker);
            viewFactory.SpawnLaserWeaponUI(laserWeapon);

            var projectileWeapon = new ProjectileWeapon(this);

            var player = new Player(playerController, laserWeapon, projectileWeapon, world);
            viewFactory.SpawnPlayerView(player);

            return player;
        }

        public Enemy CreateAsteroid(Vector3 position, Quaternion rotation)
        {
            var enemy = new Enemy(position, rotation, Vector3.one, 3.0f, world);
            viewFactory.SpawnAsteroidView(enemy);
            return enemy;
        }

        public Enemy CreateAsteroidPart(Vector3 position, Quaternion rotation)
        {
            var enemy = new Enemy(position, rotation, Vector3.one * 0.5f, 6.0f, world);
            enemy.parts = 0;

            viewFactory.SpawnPartView(enemy);
            return enemy;
        }

        public ChaserEnemy CreateChaser(Actor targetActor, Vector3 position, Quaternion rotation)
        {
            var enemy = new ChaserEnemy(targetActor, position, rotation, Vector3.one, 3.0f, world);
            viewFactory.SpawnSaucerView(enemy);
            return enemy;
        }

        public Projectile CreateProjectile(Vector3 position, Quaternion rotation)
        {
            var projectile = new Projectile(position, rotation, world);
            viewFactory.SpawnProjectileView(projectile);
            return projectile;
        }
    }
}