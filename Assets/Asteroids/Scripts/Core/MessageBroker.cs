using System.Collections.Generic;
using System;

namespace Asteroids.Core
{ 
    public class MessageBroker
    {
        private Dictionary<Type, HashSet<Action<object>>> subscribers;

        public MessageBroker()
        {
            subscribers = new Dictionary<Type, HashSet<Action<object>>>();
        }

        public void Subscribe<T>(Action<T> action)
        {
            Type messageType = typeof(T);

            if (!subscribers.ContainsKey(messageType))
            {
                subscribers[messageType] = new HashSet<Action<object>>();
            }

            subscribers[messageType].Add(obj => action((T)obj));
        }

        public void Unsubscribe<T>(Action<T> action)
        {
            Type messageType = typeof(T);

            if (subscribers.ContainsKey(messageType))
            {
                subscribers[messageType].Remove(obj => action((T)obj));

                if (subscribers[messageType].Count == 0)
                {
                    subscribers.Remove(messageType);
                }
            }
        }

        public void Publish<T>(T message)
        {
            Type messageType = typeof(T);

            if (subscribers.ContainsKey(messageType))
            {
                foreach (var action in subscribers[messageType])
                {
                    action.Invoke(message);
                }
            }
        }
    }
}
