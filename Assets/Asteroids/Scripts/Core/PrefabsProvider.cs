﻿using Asteroids.Gameplay;
using Asteroids.Gameplay.Enemies;
using Asteroids.Gameplay.Weapons;
using Asteroids.UI;
using UnityEngine;

namespace Asteroids.Core.ContentProvider
{
    // TODO: Add loading assets from Addressables.
    public class PrefabsProvider : MonoBehaviour
    {
        [SerializeField] private PlayerView playerPrefab;
        [SerializeField] private EnemyView brickPrefab;
        [SerializeField] private EnemyView brickPartPrefab;
        [SerializeField] private EnemyView saucerPrefab;
        [SerializeField] private ProjectileView projectilePrefab;
        [SerializeField] private LaserView laserPrefab;
        [SerializeField] private GameUI gameUI;
        [SerializeField] private LaserWeaponUI laserWeaponUI;

        public PlayerView GetPlayerPrefab() => playerPrefab;
        public EnemyView GetSaucerPrefab() => saucerPrefab;
        public EnemyView GetBrickPrefab() => brickPrefab;
        public EnemyView GetBrickPartPrefab() => brickPartPrefab;
        public ProjectileView GetProjectilePrefab() => projectilePrefab;
        public LaserView GetLaserView() => laserPrefab;
        public GameUI GetGameUI() => gameUI;
        public LaserWeaponUI GetLaserWeaponUI() => laserWeaponUI;
    }
}