﻿using System;
using UnityEngine;
using UnityEngine.Pool;

namespace Asteroids.Core
{
    public class PoolableObject : MonoBehaviour
    {
        public Action<PoolableObject> ReturnToPool;

        public void Release()
        {
            ReturnToPool?.Invoke(this);
        }
    }

    public class PooledObject<T> where T : PoolableObject
    {
        private readonly ObjectPool<T> pool;

        public PooledObject(T prefab)
        {
            pool = new ObjectPool<T>(
                () =>
                {
                    var obj = GameObject.Instantiate(prefab);
                    obj.ReturnToPool += ReturnToPool;
                    return (T)obj;
                },
                (obj) => { obj.gameObject.SetActive(true); },
                (obj) => { obj.gameObject.SetActive(false); },
                (obj) => { obj.gameObject.SetActive(false); });
        }

        public void ReturnToPool(PoolableObject instance)
        {
            if (instance == null)
                return;

            pool.Release((T)instance);
        }

        public T Get() => pool.Get();
    }
}