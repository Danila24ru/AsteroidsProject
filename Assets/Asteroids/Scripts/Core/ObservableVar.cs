﻿using System;

namespace Asteroids.Scripts.Core.Observable
{
    public class ObservableVar<T> where T : IEquatable<T>
    {
        public event Action<T> OnValueChanged;

        private T value;
        public T Value
        {
            get => value;
            set
            {
                if (this.value.Equals(value))
                    return;

                this.value = value;
                OnValueChanged?.Invoke(value);
            }
        }

        public ObservableVar()
        {
            value = default;
        }

        public ObservableVar(T value)
        {
            this.value = value;
        }
    }
}