﻿using Asteroids.Core;
using UnityEngine;

namespace Asteroids.Gameplay.Enemies
{
    public class EnemyView : ActorView<Enemy>
    {
        public void Update()
        {
            transform.position = actor.Position;
            transform.rotation = actor.Rotation;
            transform.localScale = actor.Scale;
        }
    }
}