﻿using Asteroids.Core;
using Asteroids.Core.Physics;
using UnityEngine;
using Asteroids.Gameplay.Weapons;
using Asteroids.Gameplay.Signals;

namespace Asteroids.Gameplay.Enemies
{

    public class Enemy : Actor
    {
        public int parts = 4;

        public float moveSpeed = 3.0f;
        public float rotationSpeed = 200.0f;

        public Enemy(Vector3 position, Quaternion rotation, Vector3 scale, float moveSpeed, World world) : base(world)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;

            this.moveSpeed = moveSpeed;
            
            var collider = AddComponent<PhysicsCollider>();
            collider.Radius = Scale.y;
            collider.Layer = 1;

            AddComponent(new PhysicsBody(Up() * moveSpeed), world);
        }

        private void RotateView(float deltaTime)
        {
            Rotation *= Quaternion.Euler(Vector3.forward * rotationSpeed * deltaTime);
        }

        public override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);

            RotateView(deltaTime);
        }

        public override void OnCollisionEnter(Actor other)
        {
            if(other is Projectile)
            {
                world.MessageBroker.Publish(new EnemyDeadSignal(this));
            }
        }
    }
}