﻿using Asteroids.Core;
using Asteroids.Core.Physics;
using UnityEngine;

namespace Asteroids.Gameplay.Enemies
{
    public class ChaserEnemy : Enemy
    {
        private readonly Actor chaseTarget;

        private PhysicsBody body;

        public ChaserEnemy(Actor chaseTarget, Vector3 position, Quaternion rotation, Vector3 scale, float moveSpeed, World world) : base(position, rotation, scale, moveSpeed, world)
        {
            this.chaseTarget = chaseTarget;

            parts = 0;

            body = GetComponent<PhysicsBody>();
        }

        public override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);

            body.Velocity = (chaseTarget.Position - Position).normalized * moveSpeed;
        }

        void RotateToTarget(float deltaTime)
        {
            Vector3 directionToTarget = chaseTarget.Position - Position;
            float angle = Mathf.Atan2(directionToTarget.y, directionToTarget.x) * Mathf.Rad2Deg;

            Quaternion targetRotation = Quaternion.Euler(0f, 0f, angle);
            float rotationStep = rotationSpeed * deltaTime;
            Rotation = Quaternion.RotateTowards(Rotation, targetRotation, rotationStep);
        }
    }
}