﻿using System;
using System.Collections.Generic;
using Asteroids.Core;
using Asteroids.Core.ContentProvider;
using Asteroids.Core.Physics;
using Asteroids.Gameplay.Signals;
using Asteroids.Gameplay.Stats;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Asteroids.Gameplay.Enemies
{

    public class EnemyManager
    {
        private readonly World world;
        private readonly Player player;
        private readonly PlayerStats playerStats;
        private readonly ActorsFactory actorsFactory;

        private float maxEnemiesAtATime = 15;
        private float spawnRate = 0.7f;
        private float nextTimeSpawn;

        private HashSet<Enemy> enemies;
        
        public EnemyManager(World world, Player player, PlayerStats playerStats, ActorsFactory actorsFactory, MessageBroker messageBroker)
        {
            this.world = world;
            this.player = player;
            this.playerStats = playerStats;
            this.actorsFactory = actorsFactory;

            enemies = new HashSet<Enemy>();

            messageBroker.Subscribe<EnemyDeadSignal>(OnEnemyDead);
        }

        public void Update(float deltaTime)
        {
            SpawnRandomEnemies();

            UpdateEnemies(deltaTime);
        }

        private void SpawnRandomEnemies()
        {
            if (Time.time < nextTimeSpawn || enemies.Count >= maxEnemiesAtATime)
                return;

            var randomRotation = Quaternion.Euler(0, 0, Random.Range(0.0f, 360.0f));

            var randomViewPortPos = new Vector3(Random.Range(0f, 1.0f), Random.Range(0f, 1.0f), 15.0f);
            var randomPoint = Camera.main.ViewportToWorldPoint(randomViewPortPos);
            randomPoint.z = 0;

            //prevent spawn enemy inside player
            if(player != null && player.IsAlive)
            {
                var distanceBetweenPlayerAndEnemy = randomPoint - player.Position;
                if (distanceBetweenPlayerAndEnemy.magnitude < 3.0f)
                {
                    randomPoint = randomPoint + distanceBetweenPlayerAndEnemy.normalized * Random.Range(10, 15);
                }
            }

            var isAsteroidSpawn = Random.value > 0.3f;

            Enemy enemy;

            if (isAsteroidSpawn)
            {
                enemy = actorsFactory.CreateAsteroid(randomPoint, randomRotation);
            }
            else
            {
                enemy = actorsFactory.CreateChaser(player, randomPoint, randomRotation);
            }

            enemies.Add(enemy);

            nextTimeSpawn = Time.time + spawnRate;
        }

        private void SpawnAsteroidPart(Vector3 position)
        {
            var randomRotation = Quaternion.Euler(0, 0, Random.Range(0.0f, 360.0f));

            var enemy = actorsFactory.CreateAsteroidPart(position, randomRotation);

            enemies.Add(enemy);
        }

        private void OnEnemyDead(EnemyDeadSignal signal)
        {
            var enemy = signal.enemy;

            playerStats.score.Value += 1;

            enemies.Remove(enemy);
            world.DestroyActor(enemy);

            for (int i = 0; i < enemy.parts; i++)
            {
                SpawnAsteroidPart(enemy.Position);
            }
        }

        private void UpdateEnemies(float deltaTime)
        {
            enemies.RemoveWhere((enemy) =>
            {
                var xPos = enemy.Position.x;
                var yPos = enemy.Position.y;
                var offset = 40.0f;

                if (xPos >= offset ||
                    xPos <= -offset ||
                    yPos >= offset ||
                    yPos <= -offset)
                {
                    world.DestroyActor(enemy);
                    return true;
                }
                return false;
            });
        }
    }
}