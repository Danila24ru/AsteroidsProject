﻿using Asteroids.Core;
using Asteroids.Core.Physics;
using System;
using UnityEngine;

namespace Asteroids.Gameplay.Weapons
{
    public class WeaponBase : ActorComponent
    {
        protected float fireRate = 0;
        private float nextFireTime = 0;

        public virtual void Fire(Vector3 startPosition, Quaternion startRotation) { }

        protected virtual bool CanFire()
        {
            if (fireRate == 0)
                return true;

            var canFire = Time.time >= nextFireTime;
            nextFireTime = Time.time + fireRate;
            return canFire;
        }

        public float GetCooldownTime() => nextFireTime > Time.time ? nextFireTime - Time.time : 0;
    }
}