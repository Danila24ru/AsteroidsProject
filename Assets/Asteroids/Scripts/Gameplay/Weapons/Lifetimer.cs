﻿using Asteroids.Core;

namespace Asteroids.Gameplay.Components
{
    public class Lifetimer : ActorComponent
    {
        public float lifetime = 5.0f;

        private float t = 0f;

        public override void OnUpdate(float deltaTime)
        {
            if(t < lifetime)
            {
                t += deltaTime;
                return;
            }

            World.DestroyActor(Owner);
        }
    }
}