﻿using Asteroids.Core;
using Asteroids.Core.Physics;
using Asteroids.Gameplay.Components;
using UnityEngine;

namespace Asteroids.Gameplay.Weapons
{
    public class Projectile : Actor
    {
        public float speed = 30.0f;
        public float lifetime = 3.0f;

        public Projectile(Vector3 position, Quaternion rotation, World world) : base(world)
        {
            Position = position;
            Rotation = rotation;

            var radius = 0.2f;
            Scale = Vector3.one * radius;

            AddComponent<PhysicsBody>();
            var collider = AddComponent<PhysicsCollider>();
            collider.Radius = radius;

            AddComponent<Lifetimer>();
        }

        public override void OnCollisionEnter(Actor other)
        {
            world.DestroyActor(this);
        }
    }
}