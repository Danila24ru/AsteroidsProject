﻿using Asteroids.Core;
using Asteroids.Core.ContentProvider;
using Asteroids.Core.Physics;
using Asteroids.Gameplay.Enemies;
using Asteroids.Gameplay.Inputs;
using Asteroids.Gameplay.Signals;
using Asteroids.Scripts.Core.Observable;
using Asteroids.UI;
using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Asteroids.Gameplay.Weapons
{
    public class LaserWeapon : WeaponBase
    {
        private readonly ViewFactory viewFactory;
        private readonly MessageBroker messageBroker;

        private LinecastResult[] linecasts;
        private int hitsCount;

        public ObservableVar<int> Ammo { get; private set; }
        public ObservableVar<float> NextLoadTime { get; private set; }

        private readonly float reloadRate = 2.0f;

        public LaserWeapon(ViewFactory viewFactory, MessageBroker messageBroker)
        {
            this.viewFactory = viewFactory;
            this.messageBroker = messageBroker;

            Ammo = new ObservableVar<int>();
            NextLoadTime = new ObservableVar<float>();

            Ammo.Value = 0;

            linecasts = new LinecastResult[10];
        }

        public override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);

            if(Time.time >= NextLoadTime.Value)
            {
                NextLoadTime.Value = Time.time + reloadRate;

                Ammo.Value++;
            }
        }

        protected override bool CanFire() => base.CanFire() && Ammo.Value > 0;

        public override void Fire(Vector3 startPosition, Quaternion startRotation)
        {
            if (!CanFire())
                return;
            
            Ammo.Value--;

            var laser = viewFactory.SpawnLaserView(startPosition, startRotation);

            var isHit = World.Physics.CastLine(startPosition, (startRotation * Vector2.up) * laser.laserLength, ref linecasts, out hitsCount);

            if (!isHit)
                return;

            for (int i = 0; i < hitsCount; i++)
            {
                var hitCollider = linecasts[i].collider;

                if (hitCollider.Owner is Enemy enemy)
                {
                    messageBroker.Publish(new EnemyDeadSignal(enemy));
                }
            }
        }
    }
}