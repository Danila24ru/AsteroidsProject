﻿using Asteroids.Core;

namespace Asteroids.Gameplay.Weapons
{
    public class ProjectileView : ActorView<Projectile>
    {
        private void Update()
        {
            transform.position = actor.Position;
            transform.rotation = actor.Rotation;
            transform.localScale = actor.Scale;
        }
    }
}