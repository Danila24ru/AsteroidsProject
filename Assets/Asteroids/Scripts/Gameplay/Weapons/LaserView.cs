using Asteroids.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Asteroids.Gameplay.Weapons
{
    public class LaserView : PoolableObject
    {
        public float laserLength = 50;
        public float lifetime = 1.0f;

        public void OnEnable()
        {
            StartCoroutine(DelayDead());
        }

        IEnumerator DelayDead()
        {
            yield return new WaitForSeconds(lifetime);
            Release();
        }
    }
}

