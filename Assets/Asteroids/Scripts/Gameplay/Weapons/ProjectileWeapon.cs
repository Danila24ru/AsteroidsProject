﻿using Asteroids.Core.ContentProvider;
using Asteroids.Core.Physics;
using System;
using UnityEngine;

namespace Asteroids.Gameplay.Weapons
{
    public class ProjectileWeapon : WeaponBase
    {
        private readonly ActorsFactory actorsFactory;
        
        public ProjectileWeapon(ActorsFactory actorsFactory)
        {
            this.actorsFactory = actorsFactory;
        }

        public override void Fire(Vector3 startPosition, Quaternion startRotation)
        {
            if (!CanFire())
                return;

            var projectile = actorsFactory.CreateProjectile(startPosition, startRotation);

            var body = projectile.GetComponent<PhysicsBody>();
            body.Velocity = (startRotation * Vector3.up) * projectile.speed;
        }
    }
}