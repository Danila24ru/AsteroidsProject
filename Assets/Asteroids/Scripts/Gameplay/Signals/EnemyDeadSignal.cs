﻿using Asteroids.Gameplay.Enemies;

namespace Asteroids.Gameplay.Signals
{
    public class EnemyDeadSignal
    {
        public Enemy enemy;

        public EnemyDeadSignal(Enemy enemy)
        {
            this.enemy = enemy;
        }
    }
}