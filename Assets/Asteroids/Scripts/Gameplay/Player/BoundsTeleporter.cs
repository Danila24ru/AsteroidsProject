

using Asteroids.Core;
using UnityEngine;

namespace Asteroids.Gameplay.Components
{
	public class BoundsTeleporter : ActorComponent
	{
		private float boundOffset = 0.01f;
		private float teleportPositionOffset = 0.02f;

        public override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);

			CheckPortals();
        }

		private void CheckPortals()
		{
			var camera = Camera.main;
			var viewportPoint = camera.WorldToViewportPoint(Owner.Position);

			if (viewportPoint.y >= 1 - boundOffset)
			{
				viewportPoint.y = teleportPositionOffset;
				viewportPoint.x = 1 - viewportPoint.x;
			}
			else if (viewportPoint.y <= boundOffset)
			{
				viewportPoint.y = 1 - teleportPositionOffset;
				viewportPoint.x = 1 - viewportPoint.x;
			}
			else if (viewportPoint.x >= 1 - boundOffset)
			{
				viewportPoint.x = teleportPositionOffset;
				viewportPoint.y = 1 - viewportPoint.y;
			}
			else if (viewportPoint.x <= boundOffset)
			{
				viewportPoint.x = 1 - teleportPositionOffset;
				viewportPoint.y = 1 - viewportPoint.y;
			}
			else
				return;

			var newPos = Camera.main.ViewportToWorldPoint(viewportPoint);
			newPos.z = 0;
			Owner.Position = newPos;
		}
	}
}