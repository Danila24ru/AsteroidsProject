﻿using Asteroids.Scripts.Core.Observable;

namespace Asteroids.Gameplay.Stats
{
    public class PlayerStats
    {
        public ObservableVar<int> score;

        public PlayerStats()
        {
            score = new ObservableVar<int>();
        }
    }
}