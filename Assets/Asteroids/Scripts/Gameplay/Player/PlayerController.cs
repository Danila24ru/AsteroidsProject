﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Asteroids.Gameplay.Inputs
{

    public class PlayerController
    {
        public readonly ShipPlayerActions inputs;

        public Action OnFirePressed;
        public Action OnSecondFirePressed;

        public Vector2 MoveDirection { get; private set; }

        public PlayerController()
        {
            inputs = new ShipPlayerActions();
            BindInputs();
        }

        private void BindInputs()
        {
            inputs.Enable();

            inputs.Player.Fire.performed += OnFireInput;
            inputs.Player.FireSecond.performed += OnSecondFireInput;
        }

        public void ReadInputs()
        {
            MoveDirection = inputs.Player.Move.ReadValue<Vector2>();
        }

        public void DisableInputs()
        {
            inputs.Disable();
        }

        private void OnSecondFireInput(InputAction.CallbackContext context)
        {
            OnSecondFirePressed?.Invoke();
        }

        private void OnFireInput(InputAction.CallbackContext context)
        {
            OnFirePressed?.Invoke();
        }
    }
}