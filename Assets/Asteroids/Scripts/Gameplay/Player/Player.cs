﻿using Asteroids.Core;
using Asteroids.Core.Physics;
using Asteroids.Gameplay.Components;
using Asteroids.Gameplay.Inputs;
using Asteroids.Gameplay.Weapons;
using UnityEngine;

namespace Asteroids.Gameplay
{
    public class Player : Actor
    {
        private float maxSpeed = 10.0f;
        private float rotationSpeed = 300.0f;
        private float acceleration = 8.0f;
        private float inertia = 5.0f;

        private PhysicsCollider collider;
        private PhysicsBody body;
        private PlayerController controller;

        private WeaponBase primaryWeapon;
        private WeaponBase secondaryWeapon;

        public bool IsAlive { get; private set; } = true;

        public Player(PlayerController playerController, WeaponBase primaryWeapon, WeaponBase secondaryWeapon, World world) : base(world)
        {
            collider = AddComponent<PhysicsCollider>();
            collider.Layer = 10;

            body = AddComponent<PhysicsBody>();
            AddComponent<BoundsTeleporter>();

            controller = playerController;

            this.primaryWeapon = AddComponent(primaryWeapon, world);
            this.secondaryWeapon = AddComponent(secondaryWeapon, world);
            
            BindInputs(playerController);
        }

        public void BindInputs(PlayerController controller)
        {
            controller.OnFirePressed += Fire;
            controller.OnSecondFirePressed += SecondFire;
        }

        public void UnbindInputs(PlayerController controller)
        {
            controller.OnFirePressed -= Fire;
            controller.OnSecondFirePressed -= SecondFire;
            controller.DisableInputs();
        }

        private void Fire()
        {
            primaryWeapon?.Fire(Position + Up() * 2, Rotation);
        }

        private void SecondFire()
        {
            secondaryWeapon?.Fire(Position + Up() * 2, Rotation);
        }

        public override void OnUpdate(float deltaTime)
        {
            MovePlayer(deltaTime);
            RotatePlayer(deltaTime);
        }

        public override void Dispose()
        {
            base.Dispose();

            UnbindInputs(controller);
            IsAlive = false;
        }

        private void MovePlayer(float deltaTime)
        {
            var moveAxis = controller.MoveDirection.y;

            if (moveAxis > 0 && body.Velocity.magnitude < maxSpeed)
                body.Velocity = body.Velocity + Up() * acceleration * deltaTime;
            else
            {
                body.Velocity = Vector3.MoveTowards(body.Velocity, Vector3.zero, inertia * deltaTime);
            }
        }

        private void RotatePlayer(float deltaTime)
        {
            var rotateDirection = -controller.MoveDirection.x;

            Rotation *= Quaternion.Euler(Vector3.forward * rotateDirection * rotationSpeed * deltaTime);
        }

        public override void OnCollisionEnter(Actor other)
        {
            if (other is Projectile)
                return;

            IsAlive = false;

            UnbindInputs(controller);
        }
    }
}