using Asteroids.Core;

namespace Asteroids.Gameplay
{
    public class PlayerView : ActorView<Player>
    {
        private void Update()
        {
            transform.position = actor.Position;
            transform.rotation = actor.Rotation;
            transform.localScale = actor.Scale;

            if (!actor.IsAlive)
            {
                gameObject.SetActive(false);

                // TODO: play destroy VFX
            }
        }
    }
}
