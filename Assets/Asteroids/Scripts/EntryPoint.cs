using Asteroids;
using Asteroids.Core.ContentProvider;
using UnityEngine;

public class EntryPoint : MonoBehaviour
{
    [SerializeField] private PrefabsProvider prefabsProvider;

    private Game game;
    
    private void Start()
    {
        game = new Game(prefabsProvider);
        game.StartGame();
    }

    private void Update()
    {
        game.Tick(Time.deltaTime);
    }
}
