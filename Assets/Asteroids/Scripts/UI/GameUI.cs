﻿using Asteroids.Core.Physics;
using Asteroids.Gameplay;
using Asteroids.Gameplay.Stats;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Asteroids.UI
{
    public class GameUI : MonoBehaviour
    {
        [SerializeField] private GameObject statsPanel;
        [SerializeField] private TextMeshProUGUI playerAngle;
        [SerializeField] private TextMeshProUGUI playerVelocity;
        [SerializeField] private TextMeshProUGUI playerPosition;

        [SerializeField] private TextMeshProUGUI score;

        [SerializeField] private GameObject resultPanel;
        [SerializeField] private TextMeshProUGUI totalScore;
        [SerializeField] private Button startAgain;

        private Game game;
        
        public void Init(Game game, PlayerStats playerStats, Player player)
        {
            this.game = game;

            playerStats.score.OnValueChanged += ScoreOnValueChanged;

            StartCoroutine(UpdateUI(playerStats, player));
        }

        private IEnumerator UpdateUI(PlayerStats playerStats, Player player)
        {
            while (player.IsAlive)
            {
                CurrentAngleValueChanged(player.Rotation.eulerAngles.z);
                CurrentVelocityValueChanged(player.GetComponent<PhysicsBody>().Velocity.magnitude);
                PlayerPositionValueChanged(player.Position);

                yield return new WaitForSeconds(0.1f);
            }

            PlayerDead();
        }

        public Button GetButtonPlayAgain() => startAgain;

        private void PlayerDead()
        {
            resultPanel.gameObject.SetActive(true);
            statsPanel.SetActive(true);
        }

        private void ScoreOnValueChanged(int score)
        {
            this.score.text = score.ToString();
            totalScore.text = score.ToString();
        }

        private void PlayerPositionValueChanged(Vector2 playerPos)
        {
            playerPosition.text = $"X: {playerPos.x.ToString("0.0")} Y: {playerPos.y.ToString("0.0")}";
        }

        private void CurrentVelocityValueChanged(float value)
        {
            playerVelocity.text = $"{value.ToString("0.0")} m/s";
        }

        private void CurrentAngleValueChanged(float value)
        {
            playerAngle.text = value.ToString("0.0");
        }
    }
}