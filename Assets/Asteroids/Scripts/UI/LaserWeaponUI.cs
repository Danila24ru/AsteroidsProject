using Asteroids.Gameplay.Weapons;
using System;
using System.Collections;
using TMPro;
using UnityEngine;


namespace Asteroids.UI
{
    public class LaserWeaponUI : MonoBehaviour, IDisposable
    {
        [SerializeField] private TextMeshProUGUI reloadTimeText;
        [SerializeField] private TextMeshProUGUI ammoText;

        private LaserWeapon laserWeapon;

        public void Init(LaserWeapon laserWeapon)
        {
            this.laserWeapon = laserWeapon;

            laserWeapon.NextLoadTime.OnValueChanged += OnNextLoadTimeChanged;
            laserWeapon.Ammo.OnValueChanged += OnAmmoChanged;
        }

        private void OnDestroy()
        {
            laserWeapon.NextLoadTime.OnValueChanged -= OnNextLoadTimeChanged;
            laserWeapon.Ammo.OnValueChanged -= OnAmmoChanged;
        }

        private void OnAmmoChanged(int ammo)
        {
            ammoText.text = $"Ammo: {ammo}";
        }

        private void OnNextLoadTimeChanged(float nextTime)
        {
            StartCoroutine(TimerAnimation(nextTime));
        }

        IEnumerator TimerAnimation(float nextTime)
        {
            while (Time.time < nextTime)
            {
                reloadTimeText.text = $"{(nextTime - Time.time).ToString("0.0")} reload.";

                yield return new WaitForSeconds(0.1f);
            }
        }
        public void Dispose()
        {
            StopAllCoroutines();
            Destroy(this.gameObject);
        }
    }
}

