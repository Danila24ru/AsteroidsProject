﻿using Asteroids.Core;
using Asteroids.Core.ContentProvider;
using Asteroids.Core.Physics;
using Asteroids.Gameplay;
using Asteroids.Gameplay.Enemies;
using Asteroids.Gameplay.Inputs;
using Asteroids.Gameplay.Stats;
using Asteroids.UI;
using UnityEngine.SceneManagement;

namespace Asteroids
{
    public class Game
    {
        private World world;

        private readonly ViewFactory viewFactory;
        private readonly ActorsFactory actorsFactory;
        private EnemyManager enemyManager;
        private MessageBroker messageBroker;

        public bool IsPlayerAlive { get; private set; } = true;

        private Player player;
        private PlayerStats stats;
        private PlayerController playerController;

        private GameUI gameUI;

        public Game(PrefabsProvider prefabsProvider)
        {
            var physics = new PhysicsEngine();
            messageBroker = new MessageBroker();

            this.world = new World(physics, messageBroker);
            this.viewFactory = new ViewFactory(prefabsProvider);
            this.actorsFactory = new ActorsFactory(viewFactory, world, messageBroker);
        }

        public void StartGame()
        {
            stats = new PlayerStats();
            playerController = new PlayerController();

            player = actorsFactory.CreateDefaultPlayer(playerController);
            
            enemyManager = new EnemyManager(world, player, stats, actorsFactory, messageBroker);

            AddUI();
        }

        private void AddUI()
        {
            gameUI = viewFactory.SpawnGameUI();
            gameUI.Init(this, stats, player);
            gameUI.GetButtonPlayAgain().onClick.AddListener(RestartGame);
        }

        public void Tick(float deltaTime)
        {
            playerController.ReadInputs();

            world.Simulate(deltaTime);

            enemyManager.Update(deltaTime);
        }

        private void RestartGame()
        {
            SceneManager.LoadScene("Asteroids");
        }
    }
}